let studentArr = ["Carl","Bea","Alex"];

function addStudent(name) {
	studentArr.push(name);
	console.log(`${name} was added to the student's list.`);
}

function countStudents(){
	let count = studentArr.length;
	console.log(`There are a total of ${count} student/s enrolled.`);
};

function printStudents(){

	studentArr.sort();
	studentArr.forEach((student) => {
		console.log(student);
	});

}

function findStudent(searchStudent) {
	
	let filtered = studentArr.filter((student) =>{
		return student.toLowerCase().includes(searchStudent.toLowerCase())
	})

	if(filtered.length === 0){
		console.log(`No student found with the name ${searchStudent}.`)
	} else if(filtered.length === 1) {
		console.log(`${filtered.toString()} is an enrollee.`)
	} else {
		console.log(`${filtered.sort().toString()} are enrollees.`)
	}
	
}